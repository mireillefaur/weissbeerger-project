import groovy.json.*
import java.util.*
import hudson.Util

def call(currentBuild) {

    try {
        node ('master') {

            stage('Checkout SCM') {
                deleteDir()
                checkout scm
            }
            stage('Build Image') {       
                env.IMAGE_TAG = env.BUILD_NUMBER
                dir( env.CI_PATH ) {                  
                    sh """
                    docker build -t "$CI_IMAGE:$IMAGE_TAG" .
                    """
                }
            }
            stage('Push Image to ECR') {
                env.CI_REGISTRY_IMAGE = env.CI_REGISTRY + '/' + env.CI_IMAGE
                sh """
                \$(aws ecr get-login --region "$env.CI_REGION" --no-include-email)
                docker tag "$CI_IMAGE:$IMAGE_TAG" "$CI_REGISTRY_IMAGE:$IMAGE_TAG"
                docker push "$CI_REGISTRY_IMAGE:$IMAGE_TAG"
		docker images --quiet --filter=dangling=true | xargs --no-run-if-empty docker rmi -f
                """
            }

            stage('Tag Image as latest'){
                sh """
                docker tag "$CI_REGISTRY_IMAGE:$IMAGE_TAG" "$CI_REGISTRY_IMAGE:latest"
                docker push "$CI_REGISTRY_IMAGE:latest"
                """
            }
            stage('Deploy Image'){
            	sh """
            	docker stop $CI_IMAGE
		docker rm $CI_IMAGE
            	docker run --name $CI_IMAGE -p 0.0.0.0:80:90 -itd "$CI_IMAGE:$IMAGE_TAG"
	 	"""
            }
        }
    } catch (e) {

        throw e
    }
}
